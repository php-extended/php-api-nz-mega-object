<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

/**
 * ApiNzMegaHierarchyNode class file.
 *
 * This class represents a node in the linked list hierarchy, which encapsulates
 * ApiNzMegaNode objects.
 *
 * Those objects are double linked lists (tree in fact, as the children part
 * is multiple and not 1-1).
 *
 * @author Anastaszor
 */
class ApiNzMegaHierarchyNode implements ApiNzMegaHierarchyNodeInterface
{
	
	/**
	 * The actual node object.
	 *
	 * @var ApiNzMegaNodeInterface
	 */
	protected ApiNzMegaNodeInterface $_innerNode;
	
	/**
	 * The parent node object.
	 *
	 * @var ?ApiNzMegaHierarchyNodeInterface
	 */
	protected ?ApiNzMegaHierarchyNodeInterface $_parent = null;
	
	/**
	 * The children node objects.
	 *
	 * @var array<string, ApiNzMegaHierarchyNodeInterface>
	 */
	protected array $_children = [];
	
	/**
	 * Builds a new ApiNzMegaHierarchyNode with the given node.
	 *
	 * @param ApiNzMegaNodeInterface $node
	 */
	public function __construct(ApiNzMegaNodeInterface $node)
	{
		$this->_innerNode = $node;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaHierarchyNodeInterface::setParent()
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function setParent(ApiNzMegaHierarchyNode $parent) : ApiNzMegaHierarchyNodeInterface
	{
		if(null !== $this->_parent)
		{
			if($this->_parent->getNode()->getNodeId()->equals($parent->getNode()->getNodeId()))
			{
				return $this;
			}
			
			throw new ApiNzMegaException('Impossible to set another parent for this node.');
		}
		
		$this->_parent = $parent;
		$parent->addChild($this);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaHierarchyNodeInterface::addChild()
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function addChild(ApiNzMegaHierarchyNode $child) : ApiNzMegaHierarchyNodeInterface
	{
		// checks if already in, if it is, do not add it once more
		// check using isset in O(1)
		if(isset($this->_children[$child->getNode()->getNodeId()->__toString()]))
		{
			return $this;
		}
		
		$this->_children[$child->getNode()->getNodeId()->__toString()] = $child;
		$child->setParent($this);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaHierarchyNodeInterface::getNode()
	 */
	public function getNode() : ApiNzMegaNodeInterface
	{
		return $this->_innerNode;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaHierarchyNodeInterface::getParent()
	 */
	public function getParent() : ?ApiNzMegaHierarchyNodeInterface
	{
		return $this->_parent;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaHierarchyNodeInterface::getChildren()
	 */
	public function getChildren() : array
	{
		return \array_values($this->_children);
	}
	
}
