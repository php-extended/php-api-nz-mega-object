<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

use DateTimeImmutable;
use DateTimeInterface;

/**
 * ApiNzMegaResponseNode class file.
 *
 * This class represents a node, as given by the API.
 *
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariableName")
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiNzMegaResponseNode implements ApiNzMegaResponseNodeInterface
{
	
	/**
	 * The id of the node.
	 *
	 * @var ApiNzMegaNodeIdInterface
	 */
	protected ApiNzMegaNodeIdInterface $_nodeId;
	
	/**
	 * The id of parent node.
	 *
	 * @var ApiNzMegaNodeIdInterface
	 */
	protected ApiNzMegaNodeIdInterface $_parentId;
	
	/**
	 * The id of the owner user.
	 *
	 * @var ApiNzMegaUserIdInterface
	 */
	protected ApiNzMegaUserIdInterface $_userId;
	
	/**
	 * The type of the node.
	 *
	 * @var integer
	 */
	protected int $_type;
	
	/**
	 *The attributes of the node, encrypted, base64.
	 *
	 * @var ApiNzMegaStringInterface
	 */
	protected ApiNzMegaStringInterface $_attributes;
	
	/**
	 * The key of the node, encrypted, base64.
	 *
	 * @var ApiNzMegaResponseKeyInterface
	 */
	protected ApiNzMegaResponseKeyInterface $_key;
	
	/**
	 * The size of the node, in octets.
	 *
	 * @var integer
	 */
	protected int $_size = 0;
	
	/**
	 * The timestamp of the last modification of the node.
	 *
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_timestamp;
	
	/**
	 * Miniature data ^\d+:\d\*\w{11}/\d+:\d\*\w{11}$.
	 *
	 * @var ?string
	 */
	protected ?string $_frontData = null;
	
	/**
	 * Builds a new ApiNzMegaResponseNode with its inner data.
	 * 
	 * @param ApiNzMegaNodeIdInterface $h
	 * @param ApiNzMegaNodeIdInterface $p
	 * @param ApiNzMegaUserIdInterface $u
	 * @param integer $t
	 * @param ApiNzMegaStringInterface $a
	 * @param ApiNzMegaResponseKeyInterface $k
	 * @param ?integer $s
	 * @param DateTimeImmutable $ts
	 * @param ?string $fa
	 */
	public function __construct(
		ApiNzMegaNodeIdInterface $h,
		ApiNzMegaNodeIdInterface $p,
		ApiNzMegaUserIdInterface $u,
		int $t,
		ApiNzMegaStringInterface $a,
		ApiNzMegaResponseKeyInterface $k,
		?int $s,
		DateTimeImmutable $ts,
		?string $fa
	) {
		$this->_nodeId = $h;
		$this->_parentId = $p;
		$this->_userId = $u;
		$this->_type = $t;
		$this->_attributes = $a;
		$this->_key = $k;
		$this->_size = (int) $s;
		$this->_timestamp = $ts;
		$this->_frontData = $fa;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseNodeInterface::getNodeId()
	 */
	public function getNodeId() : ApiNzMegaNodeIdInterface
	{
		return $this->_nodeId;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseNodeInterface::getParentNodeId()
	 */
	public function getParentNodeId() : ApiNzMegaNodeIdInterface
	{
		return $this->_parentId;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseNodeInterface::getOwnerId()
	 */
	public function getOwnerId() : ApiNzMegaUserIdInterface
	{
		return $this->_userId;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseNodeInterface::getNodeType()
	 */
	public function getNodeType() : int
	{
		return $this->_type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseNodeInterface::getNodeAttributes()
	 */
	public function getNodeAttributes() : ApiNzMegaStringInterface
	{
		return $this->_attributes;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseNodeInterface::getNodeKey()
	 */
	public function getNodeKey() : ApiNzMegaResponseKeyInterface
	{
		return $this->_key;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseNodeInterface::getNodeSize()
	 */
	public function getNodeSize() : int
	{
		return $this->_size;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseNodeInterface::getLastModifiedDatetime()
	 */
	public function getLastModifiedDatetime() : DateTimeInterface
	{
		return $this->_timestamp;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseNodeInterface::decode()
	 */
	public function decode(ApiNzMegaKeyAes128Interface $key) : ApiNzMegaNodeInterface
	{
		switch($this->getNodeType())
		{
			case ApiNzMegaNode::TYPE_FOLDER:
				return $this->decodeFolder($key);
				
			case ApiNzMegaNode::TYPE_FILE:
				return $this->decodeFile($key);
		}
		
		$message = 'Unsupported node type to decode ({t}).';
		$context = ['{t}' => $this->getNodeType()];
		
		throw new ApiNzMegaException(\strtr($message, $context), ApiNzMegaException::EINTERNAL);
	}
	
	/**
	 * Decodes a folder node.
	 *
	 * @param ApiNzMegaKeyAes128Interface $key
	 * @return ApiNzMegaNodeInterface
	 * @throws ApiNzMegaExceptionInterface
	 */
	protected function decodeFolder(ApiNzMegaKeyAes128Interface $key) : ApiNzMegaNodeInterface
	{
		$nodeKeyStr = $this->getNodeKey()->getNodeKey()->toClear();
		$nodeKeyRaw = new ApiNzMegaKeyAes128String($nodeKeyStr->__toString());
		
		$nkey = $this->decryptKey($nodeKeyRaw, $key);
		$iiv = new ApiNzMegaKeyAes128String(ApiNzMegaEndpoint::IV);
		$metaMac = null;
		/** @var ApiNzMegaKeyAes128Interface $key */
		$attributes = $this->decryptAttributes($this->getNodeAttributes(), $nkey);
		
		return new ApiNzMegaNode(
			$this->getNodeId(),
			$this->getParentNodeId(),
			$this->getOwnerId(),
			$attributes,
			$this->getNodeType(),
			$this->getNodeSize(),
			$this->getLastModifiedDatetime(),
			$nkey,
			$iiv,
			$metaMac,
		);
	}
	
	/**
	 * Decodes a file node.
	 * 
	 * @param ApiNzMegaKeyAes128Interface $key
	 * @return ApiNzMegaNodeInterface
	 * @throws ApiNzMegaExceptionInterface
	 */
	protected function decodeFile(ApiNzMegaKeyAes128Interface $key) : ApiNzMegaNodeInterface
	{
		$nodeKeyStr = $this->getNodeKey()->getNodeKey()->toClear();
		$nodeKeyRaw = new ApiNzMegaKeyAes256String($nodeKeyStr->__toString());
		
		$decrypted256 = $this->decryptKey256($nodeKeyRaw, $key);
		$nkey = $decrypted256->reduceAes128();
		$iiv = $decrypted256->getInitializationVector();
		$metaMac = $decrypted256->getMetaMac();
		
		$attributes = $this->decryptAttributes($this->getNodeAttributes(), $nkey);
		
		return new ApiNzMegaNode(
			$this->getNodeId(),
			$this->getParentNodeId(),
			$this->getOwnerId(),
			$attributes,
			$this->getNodeType(),
			$this->getNodeSize(),
			$this->getLastModifiedDatetime(),
			$nkey,
			$iiv,
			$metaMac,
		);
	}
	
	/**
	 * Decrypts a 128 bits AES key encrypted with another 128 bits AES key,
	 * i.e. decrypts the children key for a folder with the parent's key.
	 *
	 * @param ApiNzMegaKeyAes128Interface $encodedKeyToDecode
	 * @param ApiNzMegaKeyAes128Interface $encodingKey
	 * @return ApiNzMegaKeyAes128Interface the encoded_key in decoded form
	 * @throws ApiNzMegaExceptionInterface
	 */
	protected function decryptKey(ApiNzMegaKeyAes128Interface $encodedKeyToDecode, ApiNzMegaKeyAes128Interface $encodingKey) : ApiNzMegaKeyAes128Interface
	{
		$decodedKey = \openssl_decrypt(
			$encodedKeyToDecode->toRawString()->__toString(),
			'AES-128-CBC',
			$encodingKey->toRawString()->__toString(),
			\OPENSSL_NO_PADDING,
			ApiNzMegaEndpoint::IV,
		);
		
		if(false === $decodedKey)
		{
			throw new ApiNzMegaException((string) \openssl_error_string(), ApiNzMegaException::EKEY);
		}
		
		return new ApiNzMegaKeyAes128String($decodedKey);
	}
	
	/**
	 * Decrypts a 256 bits AES key encrypted with a 128 bits AES key, i.e.
	 * decrypts the children key for a file with the parent's folder key.
	 *
	 * @param ApiNzMegaKeyAes256Interface $encodedKeyToDecode
	 * @param ApiNzMegaKeyAes128Interface $encodingKey
	 * @return ApiNzMegaKeyAes256Interface the encoded_key in decoded form
	 * @throws ApiNzMegaExceptionInterface
	 */
	protected function decryptKey256(ApiNzMegaKeyAes256Interface $encodedKeyToDecode, ApiNzMegaKeyAes128Interface $encodingKey) : ApiNzMegaKeyAes256Interface
	{
		$rawstring = $encodedKeyToDecode->toRawString()->__toString();
		$decodedKey = '';
		
		foreach([(string) \mb_substr($rawstring, 0, 16, '8bit'), (string) \mb_substr($rawstring, 16, null, '8bit')] as $chunk)
		{
			$decodedChunk = \openssl_decrypt(
				$chunk,
				'AES-128-CBC',
				$encodingKey->toRawString()->__toString(),
				\OPENSSL_NO_PADDING,
				ApiNzMegaEndpoint::IV,
			);
			
			if(false === $decodedChunk)
			{
				throw new ApiNzMegaException((string) \openssl_error_string(), ApiNzMegaException::EKEY);
			}
			
			$decodedKey .= $decodedChunk;
		}
		
		return new ApiNzMegaKeyAes256String($decodedKey);
	}
	
	/**
	 * Decrypts a string with the key, i.e. decrypts the attributes for a node
	 * with it's own key.
	 *
	 * @param ApiNzMegaStringInterface $string
	 * @param ApiNzMegaKeyAes128Interface $key
	 * @return ApiNzMegaAttributeInterface
	 * @throws ApiNzMegaExceptionInterface
	 */
	protected function decryptAttributes(ApiNzMegaStringInterface $string, ApiNzMegaKeyAes128Interface $key) : ApiNzMegaAttributeInterface
	{
		$decodedData = \openssl_decrypt(
			$string->toClear()->__toString(),
			'AES-128-CBC',
			$key->toRawString()->__toString(),
			\OPENSSL_NO_PADDING,
			ApiNzMegaEndpoint::IV,
		);
		
		if(false === $decodedData)
		{
			throw new ApiNzMegaException((string) \openssl_error_string(), ApiNzMegaException::EKEY);
		}
		
		if('MEGA{"' !== \mb_substr($decodedData, 0, 6, '8bit'))
		{
			$message = 'Impossible to decrypt attribute, do you have the right key ? (found = {data})';
			$context = ['{data}' => $decodedData];
			
			throw new ApiNzMegaException(\strtr($message, $context), ApiNzMegaException::EKEY);
		}
		
		// sometimes, there is some'\0' at the end of the string
		$fpos = \mb_strpos($decodedData, '{', 0, '8bit');
		$lpos = \mb_strrpos($decodedData, '}', 0, '8bit'); // strips all the \0 at the end
		if(false === $fpos || false === $lpos)
		{
			$message = 'Impossible to decode the json attribute data "{val}".';
			$context = ['{val}' => $decodedData];
			
			throw new ApiNzMegaException(\strtr($message, $context), ApiNzMegaException::EINTERNAL);
		}
		
		$substrData = (string) \mb_substr($decodedData, $fpos, $lpos - $fpos + 1, '8bit');
		$jsonDecoded = \json_decode($substrData, true);
		if(false === $jsonDecoded || null === $jsonDecoded || !\is_array($jsonDecoded) || !isset($jsonDecoded['n']))
		{
			$message = 'Failed to decode the json attribute data "{val}".';
			$context = ['{val}' => $substrData];
			
			throw new ApiNzMegaException(\strtr($message, $context), ApiNzMegaException::EINTERNAL);
		}
		
		/** @psalm-suppress MixedArgument */
		return new ApiNzMegaAttribute($jsonDecoded['n'], $jsonDecoded['c'] ?? null);
	}
	
}
