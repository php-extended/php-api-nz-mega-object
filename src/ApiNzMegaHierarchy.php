<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

/**
 * ApiNzMegaHierarchy class file.
 *
 * This class represents the hierarchy of nodes that are inside target folder.
 *
 * @author Anastaszor
 */
class ApiNzMegaHierarchy implements ApiNzMegaHierarchyInterface
{
	
	/**
	 * The linked-tree-node for the root node of the hierarchy.
	 *
	 * @var ApiNzMegaHierarchyNodeInterface
	 */
	protected ApiNzMegaHierarchyNodeInterface $_root;
	
	/**
	 * A hashmap of all known nodes to have insert and searching in O(1).
	 *
	 * @var array<string, ApiNzMegaHierarchyNodeInterface>
	 */
	protected array $_knownNodes = [];
	
	/**
	 * Builds a new ApiNzMegaHierarchy with the given root node.
	 *
	 * @param ApiNzMegaHierarchyNodeInterface $rootNode
	 */
	public function __construct(ApiNzMegaHierarchyNodeInterface $rootNode)
	{
		$this->_root = $rootNode;
		$this->_knownNodes[$rootNode->getNode()->getNodeId()->__toString()] = $this->_root;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaHierarchyInterface::getRoot()
	 */
	public function getRoot() : ApiNzMegaNodeInterface
	{
		return $this->_root->getNode();
	}
	
	/**
	 * Ads the given node to the hierarchy.
	 * 
	 * @param ApiNzMegaNodeInterface $node
	 * @return ApiNzMegaHierarchyInterface
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function add(ApiNzMegaNodeInterface $node) : ApiNzMegaHierarchyInterface
	{
		$alreadyin = $this->searchNode($node->getNodeId());
		if(null !== $alreadyin)
		{
			return $this;
		}
		
		$pnode = $this->searchNode($node->getParentId());
		if(null === $pnode)
		{
			$message = 'Impossible to find parent node with given id "{id}".';
			$context = ['{id}' => $node->getParentId()];
			
			throw new ApiNzMegaException(\strtr($message, $context));
		}
		
		$hnode = new ApiNzMegaHierarchyNode($node);
		$this->_knownNodes[$node->getNodeId()->__toString()] = $hnode;
		/** @phpstan-ignore-next-line */ /** @psalm-suppress ArgumentTypeCoercion */
		$hnode->setParent($pnode);
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaHierarchyInterface::get()
	 */
	public function get(ApiNzMegaNodeIdInterface $nodeId) : ?ApiNzMegaNodeInterface
	{
		$hnode = $this->searchNode($nodeId);
		if(null === $hnode)
		{
			return null;
		}
		
		return $hnode->getNode();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaHierarchyInterface::getChildren()
	 */
	public function getChildren(ApiNzMegaNodeIdInterface $nodeId) : array
	{
		$hnode = $this->searchNode($nodeId);
		if(null === $hnode)
		{
			$message = 'Impossible to find node with given id "{id}".';
			$context = ['{id}' => $nodeId];
			
			throw new ApiNzMegaException(\strtr($message, $context));
		}
		
		$children = [];
		
		foreach($hnode->getChildren() as $hchild)
		{
			$children[] = $hchild->getNode();
		}
		
		return $children;
	}
	
	/**
	 * Gets the node hierarchy node which contains the node with the given id.
	 *
	 * @param ApiNzMegaNodeIdInterface $nodeId
	 * @return ?ApiNzMegaHierarchyNodeInterface null if not found
	 */
	protected function searchNode(ApiNzMegaNodeIdInterface $nodeId) : ?ApiNzMegaHierarchyNodeInterface
	{
		if(isset($this->_knownNodes[$nodeId->getValue()]))
		{
			return $this->_knownNodes[$nodeId->getValue()];
		}
		
		// no need of recursive searching, we know it's not there
		return null;
	}
	
}
