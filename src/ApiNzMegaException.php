<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

use RuntimeException;
use Throwable;

/**
 * Exception for all interactions with the ApiNzMega API.
 *
 * @author Anastaszor
 */
class ApiNzMegaException extends RuntimeException implements ApiNzMegaExceptionInterface
{
	
	public const EUNKNOWN = 0;
	public const EINTERNAL = -1;
	public const EARGS = -2;
	public const EAGAIN = -3;
	public const ERATELIMIT = -4;
	public const EFAILED = -5;
	public const ETOOMANY = -6;
	public const ERANGE = -7;
	public const EEXPIRED = -8;
	public const ENOENT = -9;
	public const ECIRCULAR = -10;
	public const EACCESS = -11;
	public const EEXIST = -12;
	public const EINCOMPLETE = -13;
	public const EKEY = -14;
	public const ESID = -15;
	public const EBLOCKED = -16;
	public const EOVERQUOTA = -17;
	public const ETEMPUNAVAIL = -18;
	
	/**
	 * All standard error messages.
	 *
	 * @var array<integer, string>
	 */
	protected static $_stdErrMsg = [
		self::EUNKNOWN => 'An unknown error has occured. Please submit a bug report, detailing the exact circumstances in which this error occurred.',
		self::EINTERNAL => 'An internal error has occurred. Please submit a bug report, detailing the exact circumstances in which this error occurred.',
		self::EARGS => 'You have passed invalid arguments to this command',
		self::EAGAIN => 'A temporary congestion or server malfunction prevented your request from being processed. No data was altered. Retry. Retries must be spaced with exponential backoff',
		self::ERATELIMIT => 'You have exceeded your command weight per time quota. Please wait a few seconds, then try again (this should never happen in sane real-life applications)',
		self::EFAILED => 'The upload failed. Please restart it from scratch',
		self::ETOOMANY => 'Too many concurrent IP addresses are accessing this upload target URL',
		self::ERANGE => 'The upload file packet is out of range or not starting and ending on a chunk boundary',
		self::EEXPIRED => 'The upload target URL you are trying to access has expired. Please request a fresh one',
		self::ENOENT => 'Object (typically, node or user) not found',
		self::ECIRCULAR => 'Circular linkage attempted',
		self::EACCESS => 'Access violation (e.g., trying to write to a read-only share)',
		self::EEXIST => 'Trying to create an object that already exists',
		self::EINCOMPLETE => 'Trying to access an incomplete resource',
		self::EKEY => 'A decryption operation failed (never returned by the API)',
		self::ESID => 'Invalid or expired user session, please relogin',
		self::EBLOCKED => 'User blocked',
		self::EOVERQUOTA => 'Request over quota',
		self::ETEMPUNAVAIL => 'Resource temporarily not available, please try again later',
	];
	
	/**
	 * Builds a new ApiNzMegaException. This method will given a standard error message
	 * based on the code that is used.
	 *
	 * @param string $message
	 * @param integer $code
	 * @param Throwable $previous
	 * @SuppressWarnings("PHPMD.UndefinedVariable")
	 * @SuppressWarnings("PHPMD.CamelCaseVariableName")
	 */
	public function __construct(?string $message = null, int $code = -1, ?Throwable $previous = null)
	{
		if(!isset(ApiNzMegaException::$_stdErrMsg[$code]))
		{
			$code = -1;
		}
		
		if(null === $message)
		{
			$message = self::$_stdErrMsg[$code];
		}
		
		parent::__construct($message, $code, $previous);
	}
	
}
