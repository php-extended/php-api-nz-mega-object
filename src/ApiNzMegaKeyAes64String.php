<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

/**
 * ApiNzMegaKeyAes64String class file.
 *
 * This class represents a 64 bits AES key, stored into raw string format.
 *
 * @author Anastaszor
 */
class ApiNzMegaKeyAes64String implements ApiNzMegaKeyAes64Interface
{
	
	/**
	 * The key value, packed in string format.
	 *
	 * @var string
	 */
	protected string $_value;
	
	/**
	 * Builds a new ApiNzMegaKey for AES 64 bits with the given string.
	 *
	 * @param string $string
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function __construct(string $string)
	{
		if(8 !== \mb_strlen($string, '8bit'))
		{
			$message = 'The length of the given string is not 64 bits but "{n}" bits ({c} chars).';
			$context = ['{n}' => 8 * (int) \mb_strlen($string, '8bit'), '{c}' => \mb_strlen($string, '8bit')];
			
			throw new ApiNzMegaException(\strtr($message, $context));
		}
		
		$this->_value = $string;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaKeyAes64Interface::toArray32()
	 */
	public function toArray32() : ApiNzMegaKeyAes64Interface
	{
		/** @psalm-suppress MixedArgumentTypeCoercion */
		return new ApiNzMegaKeyAes64Array32(\array_values((array) \unpack('N*', $this->_value)));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaKeyAes64Interface::toRawString()
	 */
	public function toRawString() : ApiNzMegaKeyAes64Interface
	{
		return $this;
	}
	
}
