<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;

/**
 * ApiNzMegaResponseKeyParser class file.
 * 
 * @author Anastaszor
 * @extends AbstractParser<ApiNzMegaResponseKeyInterface>
 */
class ApiNzMegaResponseKeyParser extends AbstractParser
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : ApiNzMegaResponseKeyInterface
	{
		try
		{
			return new ApiNzMegaResponseKey((string) $data);
		}
		catch(ApiNzMegaExceptionInterface $exc)
		{
			throw new ParseException(ApiNzMegaResponseKeyInterface::class, $data, 0, null, -1, $exc);
		}
	}
	
}
