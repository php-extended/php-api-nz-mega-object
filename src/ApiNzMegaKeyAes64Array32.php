<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

/**
 * ApiNzMegaKeyAes64Array32 class file.
 *
 * This class represents a 64 bits AES key, stored into integer array format.
 *
 * @author Anastaszor
 */
class ApiNzMegaKeyAes64Array32 implements ApiNzMegaKeyAes64Interface
{
	
	/**
	 * The integer values, packed. Array of 2 elements.
	 *
	 * @var array{0: int, 1: int}
	 */
	protected array $_values;
	
	/**
	 * Builds a new ApiNzMegaKeyAes64Array32 with the given integer data.
	 *
	 * @param array<integer, integer> $values
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function __construct(array $values)
	{
		if(2 !== \count($values))
		{
			$message = 'Impossible to pack key with "{k}" values in array, must be 2.';
			$context = ['{k}' => \count($values)];
			
			throw new ApiNzMegaException(\strtr($message, $context));
		}
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidPropertyAssignmentValue */
		$this->_values = \array_values($values);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return \implode(',', $this->_values);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaKeyAes64Interface::toArray32()
	 */
	public function toArray32() : ApiNzMegaKeyAes64Interface
	{
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaKeyAes64Interface::toRawString()
	 */
	public function toRawString() : ApiNzMegaKeyAes64Interface
	{
		return new ApiNzMegaKeyAes64String((string) \pack('N*', ...$this->_values));
	}
	
}
