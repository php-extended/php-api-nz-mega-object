<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

/**
 * ApiNzMegaKeyAes256String class file.
 *
 * This class represents a 256 bits AES key, stored into raw string format.
 *
 * @author Anastaszor
 */
class ApiNzMegaKeyAes256String implements ApiNzMegaKeyAes256Interface
{
	
	/**
	 * The key value, packed in string format.
	 *
	 * @var string
	 */
	protected string $_value;
	
	/**
	 * Builds a new ApiNzMegaKey for AES 256 bits with the given string.
	 *
	 * @param string $string
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function __construct(string $string)
	{
		if(32 !== \mb_strlen($string, '8bit'))
		{
			$message = 'The length of the given string is not 256 bits but "{n}" bits ({c} chars).';
			$context = ['{n}' => 8 * (int) \mb_strlen($string, '8bit'), '{c}' => \mb_strlen($string, '8bit')];
			
			throw new ApiNzMegaException(\strtr($message, $context));
		}
		
		$this->_value = $string;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaKeyAes256Interface::toArray32()
	 */
	public function toArray32() : ApiNzMegaKeyAes256Interface
	{
		/** @psalm-suppress MixedArgumentTypeCoercion */
		return new ApiNzMegaKeyAes256Array32(\array_values((array) \unpack('N*', $this->_value)));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaKeyAes256Interface::toRawString()
	 */
	public function toRawString() : ApiNzMegaKeyAes256Interface
	{
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaKeyAes256Interface::reduceAes128()
	 */
	public function reduceAes128() : ApiNzMegaKeyAes128Interface
	{
		return $this->toArray32()->reduceAes128();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaKeyAes256Interface::getInitializationVector()
	 */
	public function getInitializationVector() : ApiNzMegaKeyAes128Interface
	{
		return $this->toArray32()->getInitializationVector();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaKeyAes256Interface::getMetaMac()
	 */
	public function getMetaMac() : ApiNzMegaKeyAes64Interface
	{
		return $this->toArray32()->getMetaMac();
	}
	
}
