<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

/**
 * ApiNzMegaKeyAes128String class file.
 *
 * This class represents a 128 bits AES key, stored into raw string format.
 *
 * @author Anastaszor
 */
class ApiNzMegaKeyAes128String implements ApiNzMegaKeyAes128Interface
{
	
	/**
	 * The key value, packed in string format.
	 *
	 * @var string
	 */
	protected string $_value;
	
	/**
	 * Builds a new ApiNzMegaKey for AES 128 bits with the given string.
	 *
	 * @param string $string
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function __construct(string $string)
	{
		if(16 !== \mb_strlen($string, '8bit'))
		{
			$message = 'The length of the given string is not 128 bits but "{n}" bits ({c} chars).';
			$context = ['{n}' => 8 * (int) \mb_strlen($string, '8bit'), '{c}' => \mb_strlen($string, '8bit')];
			
			throw new ApiNzMegaException(\strtr($message, $context));
		}
		
		$this->_value = $string;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaKeyAes128Interface::toArray32()
	 */
	public function toArray32() : ApiNzMegaKeyAes128Interface
	{
		/** @psalm-suppress MixedArgumentTypeCoercion */
		return new ApiNzMegaKeyAes128Array32((array) \unpack('N*', $this->_value));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaKeyAes128Interface::toRawString()
	 */
	public function toRawString() : ApiNzMegaKeyAes128Interface
	{
		return $this;
	}
	
}
