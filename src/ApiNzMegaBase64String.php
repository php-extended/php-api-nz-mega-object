<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

/**
 * ApiNzMegaBase64String class file.
 *
 * This class represents a string in base64 format.
 *
 * @author Anastaszor
 */
class ApiNzMegaBase64String implements ApiNzMegaStringInterface
{
	
	/**
	 * The value, a string encoded base64.
	 *
	 * @var string
	 */
	protected string $_value;
	
	/**
	 * Builds a new ApiNzMegaBase64String base64.
	 *
	 * @param string $string
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function __construct(string $string)
	{
		if(empty($string))
		{
			throw new ApiNzMegaException('The given value is empty.');
		}
		
		// mega specific additional encoding
		$string = \str_replace(['-', '_', ','], ['+', '/', ''], $string);
		$modlen = (int) \mb_strlen($string, '8bit') % 4;
		
		if(2 === $modlen)
		{
			$string .= '==';
		}
		if(3 === $modlen)
		{
			$string .= '=';
		}
		
		if(!\preg_match('#^([A-Za-z0-9+/]{4})+([A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$#', $string))
		{
			$message = 'The given value is not base64 encoded ("{val}").';
			$context = ['{val}' => $string];
			
			throw new ApiNzMegaException(\strtr($message, $context));
		}
		
		$this->_value = $string;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaStringInterface::toBase64()
	 */
	public function toBase64() : ApiNzMegaStringInterface
	{
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaStringInterface::toClear()
	 */
	public function toClear() : ApiNzMegaStringInterface
	{
		return new ApiNzMegaClearString((string) \base64_decode($this->_value, true));
	}
	
}
