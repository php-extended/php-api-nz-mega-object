<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

/**
 * ApiNzMegaAttribute class file.
 *
 * This class represents the attributes of a node.
 *
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariableName")
 */
class ApiNzMegaAttribute implements ApiNzMegaAttributeInterface
{
	
	/**
	 * The name of the node.
	 *
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The value of the attribute.
	 *
	 * @var ?string
	 */
	protected ?string $_data = null;
	
	/**
	 * Builds a new ApiNzMegaAttribute with its inner data.
	 * 
	 * @param string $n
	 * @param ?string $c
	 */
	public function __construct(string $n, ?string $c)
	{
		$this->_name = $n;
		$this->_data = $c;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaAttributeInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaAttributeInterface::getData()
	 */
	public function getData() : ?string
	{
		return $this->_data;
	}
	
}
