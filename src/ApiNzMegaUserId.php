<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

/**
 * ApiNzMegaUserId class file.
 *
 * This class represents the id in which all requests are.
 *
 * @author Anastaszor
 */
class ApiNzMegaUserId implements ApiNzMegaUserIdInterface
{
	
	/**
	 * The value of the id.
	 *
	 * @var string
	 */
	protected string $_value;
	
	/**
	 * Builds a new ApiNzMegaFolderId from the given string.
	 *
	 * @param string $identifier
	 * @throws ApiNzMegaException if the given id is not recevable
	 */
	public function __construct(string $identifier)
	{
		if(!\preg_match('#^[_a-zA-Z0-9-]{11}$#', $identifier))
		{
			$message = 'Invalid user id "{id}".';
			$context = ['{id}' => $identifier];
			
			throw new ApiNzMegaException(\strtr($message, $context));
		}
		
		$this->_value = $identifier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaUserIdInterface::getValue()
	 */
	public function getValue() : string
	{
		return $this->_value;
	}
	
}
