<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

use Psr\Http\Message\UriInterface;

/**
 * ApiNzMegaEncryptedFileLocation class file.
 *
 * This class is the response of the api in the request of a temporary link
 * to download a file.
 *
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariableName")
 */
class ApiNzMegaResponseLocation implements ApiNzMegaResponseLocationInterface
{
	
	/**
	 * The size of the file.
	 * 
	 * @var integer
	 */
	protected int $_size;
	
	/**
	 * The encrypted file name of the file.
	 * 
	 * @var ApiNzMegaStringInterface
	 */
	protected ApiNzMegaStringInterface $_fileName;
	
	/**
	 * Some metadata ?
	 * 
	 * @var integer
	 */
	protected int $_msd;
	
	/**
	 * Additional attributes.
	 * 
	 * @var ?string
	 */
	protected ?string $_fa = null;
	
	/**
	 * The time to live ?
	 * 
	 * @var integer
	 */
	protected int $_tl;
	
	/**
	 * The domain of the target file.
	 * 
	 * @var UriInterface
	 */
	protected UriInterface $_targetUri;
	
	/**
	 * The ip addresses of the domain to resolve [ipv4, ipv6].
	 * 
	 * @var array<integer, string>
	 */
	protected array $_ipAddresses = [];
	
	/**
	 * Builds a new ApiNzMegaEncryptedFileLocation with its inner values.
	 * 
	 * @param integer $s
	 * @param ApiNzMegaStringInterface $at
	 * @param integer $msd
	 * @param string $fa
	 * @param integer $tl
	 * @param UriInterface $g
	 * @param array<integer|string, string> $ip
	 */
	public function __construct(
		int $s,
		ApiNzMegaStringInterface $at,
		int $msd,
		?string $fa,
		int $tl,
		UriInterface $g,
		array $ip
	) {
		$this->_size = $s;
		$this->_fileName = $at;
		$this->_msd = $msd;
		$this->_fa = $fa;
		$this->_tl = $tl;
		$this->_targetUri = $g;
		$this->_ipAddresses = \array_values($ip);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseLocationInterface::getSize()
	 */
	public function getSize() : int
	{
		return $this->_size;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseLocationInterface::getFileName()
	 */
	public function getFileName() : ApiNzMegaStringInterface
	{
		return $this->_fileName;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseLocationInterface::getMsd()
	 */
	public function getMsd() : int
	{
		return $this->_msd;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseLocationInterface::getFa()
	 */
	public function getFa() : ?string
	{
		return $this->_fa;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseLocationInterface::getTimeToLive()
	 */
	public function getTimeToLive() : int
	{
		return $this->_tl;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseLocationInterface::getTargetUri()
	 */
	public function getTargetUri() : UriInterface
	{
		return $this->_targetUri;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseLocationInterface::getIpAddresses()
	 */
	public function getIpAddresses() : array
	{
		return $this->_ipAddresses;
	}
	
}
