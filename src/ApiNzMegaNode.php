<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

use DateTimeInterface;

/**
 * ApiNzMegaNode class file.
 *
 * This class represents a node in mega's file and folders hierarchy. A node
 * represents metadata for a folder, a file, or other specific elements.
 *
 * @author Anastaszor
 */
class ApiNzMegaNode implements ApiNzMegaNodeInterface
{
	
	public const TYPE_FILE = 0;
	public const TYPE_FOLDER = 1;
	public const TYPE_ROOT = 2;
	public const TYPE_INBOX = 3;
	public const TYPE_TRASH = 4;
	public const TYPE_CONTACT = 8;
	public const TYPE_NETWORK = 9;
	
	/**
	 * The id of this node.
	 *
	 * @var ApiNzMegaNodeIdInterface
	 */
	protected ApiNzMegaNodeIdInterface $_nodeId;
	
	/**
	 * The id of parent node.
	 *
	 * @var ApiNzMegaNodeIdInterface
	 */
	protected ApiNzMegaNodeIdInterface $_parentNodeId;
	
	/**
	 * The id of owner user.
	 *
	 * @var ApiNzMegaUserIdInterface
	 */
	protected ApiNzMegaUserIdInterface $_ownerId;
	
	/**
	 * The attributes of this node.
	 *
	 * @var ApiNzMegaAttributeInterface
	 */
	protected ApiNzMegaAttributeInterface $_attributes;
	
	/**
	 * The type of this node. One of the ApiNzMegaNode::TYPE_* constants.
	 *
	 * @var integer
	 */
	protected int $_nodeType;
	
	/**
	 * The size of this node, in octets.
	 *
	 * @var integer
	 */
	protected int $_nodeSize;
	
	/**
	 * The date when this node was last modified.
	 *
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_lastModifiedDate;
	
	/**
	 * The protected key to decrypt this node's content.
	 *
	 * @var ApiNzMegaKeyAes128Interface
	 */
	protected ApiNzMegaKeyAes128Interface $_nodeKey;
	
	/**
	 * The initialization vector, to decrypt this node's content.
	 *
	 * @var ApiNzMegaKeyAes128Interface
	 */
	protected ApiNzMegaKeyAes128Interface $_initVec;
	
	/**
	 * The meta mac, to check the integrity of this node's content.
	 *
	 * @var ?ApiNzMegaKeyAes64Interface
	 */
	protected ?ApiNzMegaKeyAes64Interface $_metaMac = null;
	
	/**
	 * Builds a new ApiNzMegaNode with the right elements.
	 *
	 * @param ApiNzMegaNodeIdInterface $nodeId
	 * @param ApiNzMegaNodeIdInterface $parentNodeId
	 * @param ApiNzMegaUserIdInterface $ownerId
	 * @param ApiNzMegaAttributeInterface $attributes
	 * @param integer $nodeType
	 * @param integer $nodeSize
	 * @param DateTimeInterface $lastModifiedDate
	 * @param ApiNzMegaKeyAes128Interface $nodeKey
	 * @param ApiNzMegaKeyAes128Interface $initVec
	 * @param ?ApiNzMegaKeyAes64Interface $metaMac
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(
		ApiNzMegaNodeIdInterface $nodeId,
		ApiNzMegaNodeIdInterface $parentNodeId,
		ApiNzMegaUserIdInterface $ownerId,
		ApiNzMegaAttributeInterface $attributes,
		int $nodeType,
		int $nodeSize,
		DateTimeInterface $lastModifiedDate,
		ApiNzMegaKeyAes128Interface $nodeKey,
		ApiNzMegaKeyAes128Interface $initVec,
		?ApiNzMegaKeyAes64Interface $metaMac = null
	) {
		$this->_nodeId = $nodeId;
		$this->_parentNodeId = $parentNodeId;
		$this->_ownerId = $ownerId;
		$this->_attributes = $attributes;
		$this->_nodeType = $nodeType;
		$this->_nodeSize = $nodeSize;
		$this->_lastModifiedDate = $lastModifiedDate;
		$this->_nodeKey = $nodeKey;
		$this->_initVec = $initVec;
		$this->_metaMac = $metaMac;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaNodeInterface::getNodeId()
	 */
	public function getNodeId() : ApiNzMegaNodeIdInterface
	{
		return $this->_nodeId;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaNodeInterface::getParentId()
	 */
	public function getParentId() : ApiNzMegaNodeIdInterface
	{
		return $this->_parentNodeId;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaNodeInterface::getOwnerId()
	 */
	public function getOwnerId() : ApiNzMegaUserIdInterface
	{
		return $this->_ownerId;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaNodeInterface::getAttributes()
	 */
	public function getAttributes() : ApiNzMegaAttributeInterface
	{
		return $this->_attributes;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaNodeInterface::getNodeType()
	 */
	public function getNodeType() : int
	{
		return $this->_nodeType;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaNodeInterface::getNodeSize()
	 */
	public function getNodeSize() : int
	{
		return $this->_nodeSize;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaNodeInterface::getLastModifiedDate()
	 */
	public function getLastModifiedDate() : DateTimeInterface
	{
		return $this->_lastModifiedDate;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaNodeInterface::getNodeKey()
	 */
	public function getNodeKey() : ApiNzMegaKeyAes128Interface
	{
		return $this->_nodeKey;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaNodeInterface::getInitializationVector()
	 */
	public function getInitializationVector() : ApiNzMegaKeyAes128Interface
	{
		return $this->_initVec;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaNodeInterface::getMetaMac()
	 */
	public function getMetaMac() : ?ApiNzMegaKeyAes64Interface
	{
		return $this->_metaMac;
	}
	
}
