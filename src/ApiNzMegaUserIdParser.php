<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;

/**
 * ApiNzMegaUserIdParser class file.
 * 
 * @author Anastaszor
 * @extends AbstractParser<ApiNzMegaUserIdInterface>
 */
class ApiNzMegaUserIdParser extends AbstractParser
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : ApiNzMegaUserIdInterface
	{
		try
		{
			return new ApiNzMegaUserId((string) $data);
		}
		catch(ApiNzMegaExceptionInterface $exc)
		{
			throw new ParseException(ApiNzMegaUserIdInterface::class, $data, 0, null, -1, $exc);
		}
	}
	
}
