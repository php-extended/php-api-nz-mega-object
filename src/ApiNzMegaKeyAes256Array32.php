<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

/**
 * ApiNzMegaKeyAes256Array32 class file.
 *
 * This class represents a 256 bits AES key, stored into integer array format.
 *
 * @author Anastaszor
 */
class ApiNzMegaKeyAes256Array32 implements ApiNzMegaKeyAes256Interface
{
	
	/**
	 * The integer values, packed. Array of 8 elements.
	 *
	 * @var array{0: int, 1: int, 2: int, 3: int, 4: int, 5: int, 6: int, 7: int}
	 */
	protected array $_values;
	
	/**
	 * Builds a new ApiNzMegaKeyAes256Array32 with the given integer data.
	 *
	 * @param array<integer, integer> $values
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function __construct(array $values)
	{
		if(8 !== \count($values))
		{
			$message = 'Impossible to pack key with "{k}" values in array, must be 8.';
			$context = ['{k}' => \count($values)];
			
			throw new ApiNzMegaException(\strtr($message, $context));
		}
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidPropertyAssignmentValue */
		$this->_values = \array_values($values);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return \implode(',', $this->_values);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaKeyAes256Interface::toArray32()
	 */
	public function toArray32() : ApiNzMegaKeyAes256Interface
	{
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaKeyAes256Interface::toRawString()
	 */
	public function toRawString() : ApiNzMegaKeyAes256Interface
	{
		return new ApiNzMegaKeyAes256String((string) \pack('N*', ...$this->_values));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaKeyAes256Interface::reduceAes128()
	 */
	public function reduceAes128() : ApiNzMegaKeyAes128Interface
	{
		return new ApiNzMegaKeyAes128Array32([
			$this->_values[0] ^ $this->_values[4],
			$this->_values[1] ^ $this->_values[5],
			$this->_values[2] ^ $this->_values[6],
			$this->_values[3] ^ $this->_values[7],
		]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaKeyAes256Interface::getInitializationVector()
	 */
	public function getInitializationVector() : ApiNzMegaKeyAes128Interface
	{
		return new ApiNzMegaKeyAes128Array32([$this->_values[4], $this->_values[5], 0, 0]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaKeyAes256Interface::getMetaMac()
	 */
	public function getMetaMac() : ApiNzMegaKeyAes64Interface
	{
		return new ApiNzMegaKeyAes64Array32([$this->_values[6], $this->_values[7]]);
	}
	
}
