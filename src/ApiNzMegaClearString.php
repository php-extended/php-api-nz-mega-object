<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

/**
 * ApiNzMegaClearString class file.
 *
 * This class represents a string in pure format.
 *
 * @author Anastaszor
 */
class ApiNzMegaClearString implements ApiNzMegaStringInterface
{
	
	/**
	 * The value, a clear string.
	 *
	 * @var string
	 */
	protected string $_value;
	
	/**
	 * Builds a new ApiNzMegaClearString.
	 *
	 * @param string $string
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function __construct(string $string)
	{
		if(empty($string))
		{
			throw new ApiNzMegaException('The given value is empty.');
		}
		
		$this->_value = $string;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaStringInterface::toBase64()
	 */
	public function toBase64() : ApiNzMegaStringInterface
	{
		return new ApiNzMegaBase64String(\base64_encode($this->_value));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaStringInterface::toClear()
	 */
	public function toClear() : ApiNzMegaStringInterface
	{
		return $this;
	}
	
}
