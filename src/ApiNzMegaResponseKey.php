<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

/**
 * ApiNzMegaResponseKey class file.
 *
 * This class represents the key and the node id of the node that contains
 * the key to decode this node's key.
 *
 * @author Anastaszor
 */
class ApiNzMegaResponseKey implements ApiNzMegaResponseKeyInterface
{
	
	/**
	 * The id of the node which contains the key to decode this node's key.
	 *
	 * @var ApiNzMegaNodeId
	 */
	protected ApiNzMegaNodeId $_nodeId;
	
	/**
	 * The encrypted key, in base64 string.
	 *
	 * @var ApiNzMegaBase64String
	 */
	protected ApiNzMegaBase64String $_nodeKey;
	
	/**
	 * Builds a new ApiNzMegaResponseKey with the string given by the API.
	 *
	 * @param string $string
	 * @throws ApiNzMegaExceptionInterface
	 */
	public function __construct(string $string)
	{
		$pos = \mb_strpos($string, ':');
		if(false === $pos)
		{
			$message = 'Impossible to find semicolon from string "{key}".';
			$context = ['{key}' => $string];
			
			throw new ApiNzMegaException(\strtr($message, $context));
		}
		
		$this->_nodeId = new ApiNzMegaNodeId((string) \mb_substr($string, 0, $pos, '8bit'));
		$this->_nodeKey = new ApiNzMegaBase64String((string) \mb_substr($string, $pos + 1, null, '8bit'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseKeyInterface::getNodeId()
	 */
	public function getNodeId() : ApiNzMegaNodeIdInterface
	{
		return $this->_nodeId;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseKeyInterface::getNodeKey()
	 */
	public function getNodeKey() : ApiNzMegaStringInterface
	{
		return $this->_nodeKey;
	}
	
}
