<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

use InvalidArgumentException;
use PhpExtended\DataProvider\JsonStringDataProvider;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use RuntimeException;
use Throwable;

/**
 * ApiNzMega class file.
 *
 * This class represents the API to get resources and contents from ApiNzMega urls.
 *
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @psalm-suppress PropertyNotSetInConstructor
 */
class ApiNzMegaEndpoint implements ApiNzMegaEndpointInterface
{
	
	public const SERVER_GLOBAL = 'https://g.api.mega.co.nz/';
	public const SERVER_EUROPE = 'https://eu.api.mega.co.nz/';
	public const IV = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
	
	/**
	 * The default server, for unspecified new ApiNzMega objects.
	 *
	 * @var string one of ApiNzMega::SERVER_* constants
	 */
	protected static string $_defaultServer = self::SERVER_GLOBAL;
	
	/**
	 * Sets the default server for each time a new connection is required.
	 *
	 * @param string $server one of ApiNzMega::SERVER_* constants
	 */
	public static function setDefaultServer(string $server) : void
	{
		switch($server)
		{
			case self::SERVER_EUROPE:
			case self::SERVER_GLOBAL:
				self::$_defaultServer = $server;
		}
	}
	
	/**
	 * Gets the default server hostname to use.
	 * 
	 * @return string
	 */
	public static function getDefaultServer() : string
	{
		return static::$_defaultServer;
	}
	
	/**
	 * The http client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 * 
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 * 
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The stream factory.
	 * 
	 * @var StreamFactoryInterface
	 */
	protected StreamFactoryInterface $_streamFactory;
	
	/**
	 * The reifier.
	 * 
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * The folder id where the connection lands.
	 *
	 * @var ApiNzMegaNodeId
	 */
	protected ApiNzMegaNodeId $_containerId;
	
	/**
	 * The decryption key where for the folder.
	 *
	 * @var ApiNzMegaKeyAes128Interface
	 */
	protected ApiNzMegaKeyAes128Interface $_containerKey;
	
	/**
	 * The node hierarchy for this target folder.
	 *
	 * @var ?ApiNzMegaHierarchy
	 */
	protected ?ApiNzMegaHierarchy $_hierarchy = null;
	
	/**
	 * The sequence number.
	 *
	 * @var integer
	 */
	protected int $_seqno;
	
	/**
	 * The user session id.
	 *
	 * @var string
	 */
	protected string $_userSessionId;
	
	/**
	 * Builds a new ApiNzMegaEndpoint with the given http json endpoint and the
	 * fragment of the url that contains the folder id and the base key data.
	 * Traditionnally, the url fragment begins at the # character.
	 * 
	 * @param string $urlFragment
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?StreamFactoryInterface $streamFactory
	 * @param ?ReifierInterface $reifier
	 * @throws ApiNzMegaExceptionInterface
	 * @throws InvalidArgumentException
	 */
	public function __construct(
		string $urlFragment,
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?StreamFactoryInterface $streamFactory = null,
		?ReifierInterface $reifier = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_streamFactory = $streamFactory ?? new StreamFactory();
		$this->_reifier = $reifier ?? new Reifier();
		
		$matches = [];
		if(!\preg_match('@^/?([a-zA-Z0-9]{8})#([a-zA-Z0-9_,\\-]{22})@', $urlFragment, $matches))
		{
			$message = 'Failed to parse fragment url fragment value "{frg}"';
			$context = ['{frg}' => $urlFragment];
			
			throw new ApiNzMegaException(\strtr($message, $context), ApiNzMegaException::EARGS);
		}
		
		/** @phpstan-ignore-next-line */
		$this->_containerId = new ApiNzMegaNodeId($matches[1] ?? '');
		/** @phpstan-ignore-next-line */
		$keyB64 = new ApiNzMegaBase64String($matches[2] ?? '');
		$this->_containerKey = new ApiNzMegaKeyAes128String($keyB64->toClear()->__toString());
		$this->_seqno = -(\mt_rand(0, \PHP_INT_MAX) & 0x7FFFFFFF); // reduction 32 bits
		
		$configuration = $this->_reifier->getConfiguration();
		$configuration->setImplementation(ApiNzMegaAttributeInterface::class, ApiNzMegaAttribute::class);
		$configuration->setImplementation(ApiNzMegaStringInterface::class, ApiNzMegaBase64String::class);
		$configuration->setImplementation(ApiNzMegaHierarchyInterface::class, ApiNzMegaHierarchy::class);
		$configuration->setImplementation(ApiNzMegaHierarchyNodeInterface::class, ApiNzMegaHierarchyNode::class);
		$configuration->setImplementation(ApiNzMegaKeyAes128Interface::class, ApiNzMegaKeyAes128String::class);
		$configuration->setImplementation(ApiNzMegaKeyAes256Interface::class, ApiNzMegaKeyAes256String::class);
		$configuration->setImplementation(ApiNzMegaKeyAes64Interface::class, ApiNzMegaKeyAes64String::class);
		$configuration->setImplementation(ApiNzMegaNodeInterface::class, ApiNzMegaNode::class);
		$configuration->setImplementation(ApiNzMegaNodeIdInterface::class, ApiNzMegaNodeId::class);
		$configuration->setImplementation(ApiNzMegaResponseKeyInterface::class, ApiNzMegaResponseKey::class);
		$configuration->setImplementation(ApiNzMegaResponseLocationInterface::class, ApiNzMegaResponseLocation::class);
		$configuration->setImplementation(ApiNzMegaResponseNodeInterface::class, ApiNzMegaResponseNode::class);
		$configuration->setImplementation(ApiNzMegaResponseNodelistInterface::class, ApiNzMegaResponseNodelist::class);
		$configuration->setImplementation(ApiNzMegaUserIdInterface::class, ApiNzMegaUserId::class);
		$configuration->setIterableInnerType(ApiNzMegaResponseNodelist::class, 'f', ApiNzMegaResponseNodeInterface::class);
		$configuration->setIterableInnerType(ApiNzMegaResponseLocation::class, 'ip', 'string');
		
		$configuration->setParser(ApiNzMegaStringInterface::class, new ApiNzMegaBase64StringParser());
		$configuration->setParser(ApiNzMegaNodeIdInterface::class, new ApiNzMegaNodeIdParser());
		$configuration->setParser(ApiNzMegaResponseKeyInterface::class, new ApiNzMegaResponseKeyParser());
		$configuration->setParser(ApiNzMegaUserIdInterface::class, new ApiNzMegaUserIdParser());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaEndpointInterface::getRootNodeInfo()
	 */
	public function getRootNodeInfo() : ApiNzMegaNodeInterface
	{
		return $this->getHierarchy()->getRoot();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaEndpointInterface::getFileInfo()
	 */
	public function getFileInfo(ApiNzMegaNodeIdInterface $nodeId) : ?ApiNzMegaNodeInterface
	{
		return $this->getHierarchy()->get($nodeId);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaEndpointInterface::getChildren()
	 */
	public function getChildren(ApiNzMegaNodeInterface $node) : array
	{
		return $this->getHierarchy()->getChildren($node->getNodeId());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaEndpointInterface::downloadFile()
	 */
	public function downloadFile(ApiNzMegaNodeInterface $node) : string
	{
		if(ApiNzMegaNode::TYPE_FILE !== $node->getNodeType())
		{
			throw new ApiNzMegaException('Only files are downloadable.', ApiNzMegaException::EINTERNAL);
		}
		
		$url = self::$_defaultServer.'cs?id='.((string) ($this->_seqno++));
		
		if(!empty($this->_userSessionId))
		{
			$url .= '&sid='.$this->_userSessionId;
		}
		
		$url .= '&n='.$this->_containerId->__toString();
		$url .= '&lang=en&domain=meganz';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('POST', $uri);
			$request = $request->withAddedHeader('Content-Type', 'text/plain;charset=UTF-8');
			$request = $request->withAddedHeader('Referer', 'https://mega.nz/folder/'.$this->_containerId->getValue());
			$request = $request->withBody($this->_streamFactory->createStream((string) \json_encode([
				'a' => 'g', // action = get
				'g' => 1,   // ???
				'n' => $node->getNodeId()->getValue(), // node id
				'ssl' => 2, // enabled ?
			])));
			$httpResponse = $this->_httpClient->sendRequest($request);
			$json = new JsonStringDataProvider($httpResponse->getBody()->__toString());
			$response = $this->_reifier->reifyAll(ApiNzMegaResponseLocationInterface::class, $json->provideAll());
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from mega at url {url}';
			$context = ['{url}' => $url];
			
			throw new ApiNzMegaException(\strtr($message, $context), ApiNzMegaException::EARGS, $exc);
		}
		
		/** @var ?ApiNzMegaResponseLocationInterface $location */
		$location = null;
		
		foreach($response as $innerLocation)
		{
			$location = $innerLocation;
		}
		
		if(null === $location)
		{
			$message = 'Failed to get location data from mega at url {url}';
			$context = ['{url}' => $url];
			
			throw new ApiNzMegaException(\strtr($message, $context), ApiNzMegaException::EARGS);
		}
		
		$request = $this->_requestFactory->createRequest('GET', $location->getTargetUri());
		$response = $this->_httpClient->sendRequest($request);
		$encodedData = $response->getBody()->__toString();
		
		$str = \openssl_decrypt(
			$encodedData,
			'AES-128-CTR',
			$node->getNodeKey()->toRawString()->__toString(),
			\OPENSSL_RAW_DATA,
			$node->getInitializationVector()->toRawString()->__toString(),
		);
		if(false === $str)
		{
			throw new ApiNzMegaException((string) \openssl_error_string(), ApiNzMegaException::EKEY);
		}
		
		return $str;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaEndpointInterface::downloadSynchronize()
	 */
	public function downloadSynchronize(ApiNzMegaNodeInterface $node, string $localPath) : int
	{
		$count = 0;
		
		$filepath = $localPath.\DIRECTORY_SEPARATOR.$node->getAttributes()->getName();
		if($node->getNodeType() === ApiNzMegaNode::TYPE_FOLDER)
		{
			if(!\is_dir($filepath))
			{
				$res = \mkdir($filepath);
				if(false === $res)
				{
					$message = 'Impossible to make directory "{path}"';
					$context = ['{path}' => $filepath];
					
					throw new RuntimeException(\strtr($message, $context));
				}
			}
			
			foreach($this->getChildren($node) as $childNode)
			{
				$count += $this->downloadSynchronize($childNode, $filepath);
			}
		}
		
		if($node->getNodeType() === ApiNzMegaNode::TYPE_FILE)
		{
			if(!\is_file($filepath))
			{
				$data = $this->downloadFile($node);
				$res = \file_put_contents($filepath, $data, \LOCK_EX);
				if(false === $res)
				{
					$message = 'Impossible to write contents at "'.$filepath.'"';
					$context = ['{path}' => $filepath];
					
					throw new RuntimeException(\strtr($message, $context));
				}
				$count++;
			}
		}
		
		return $count;
	}
	
	/**
	 * Gets the hierarchy for the given url fragment.
	 * 
	 * @return ApiNzMegaHierarchyInterface
	 * @throws ApiNzMegaExceptionInterface
	 */
	protected function getHierarchy() : ApiNzMegaHierarchyInterface
	{
		if(null !== $this->_hierarchy)
		{
			return $this->_hierarchy;
		}
		
		$url = self::$_defaultServer.'cs?id='.((string) $this->_seqno++);
		
		if(!empty($this->_userSessionId))
		{
			$url .= '&sid='.$this->_userSessionId;
		}
		
		$url .= '&n='.$this->_containerId->getValue();
		$url .= '&v=2';
		
		try
		{
			$uri = $this->_uriFactory->createUri($url);
			$request = $this->_requestFactory->createRequest('POST', $uri);
			$request = $request->withAddedHeader('Content-Type', 'text/plain;charset=UTF-8');
			$request = $request->withAddedHeader('Referer', 'https://mega.nz/folder/'.$this->_containerId->getValue());
			$request = $request->withAddedHeader('Origin', 'https://mega.nz');
			$request = $request->withAddedHeader('Host', static::getDefaultServer());
			$request = $request->withBody($this->_streamFactory->createStream((string) \json_encode([[
				'a' => 'f', // action = folder
				'c' => 1,   // ???
				'r' => 1,   // recursive
				'ca' => 1,  // ???
			]])));
			$httpResponse = $this->_httpClient->sendRequest($request);
			$json = new JsonStringDataProvider($httpResponse->getBody()->__toString());
			/** @var array<integer, ApiNzMegaResponseNodelistInterface> $response */
			$response = $this->_reifier->reifyAll(ApiNzMegaResponseNodelistInterface::class, $json->provideAll());
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get data from mega at url {url}';
			$context = ['{url}' => $url];
			
			throw new ApiNzMegaException(\strtr($message, $context), ApiNzMegaException::EARGS, $exc);
		}
		
		/** @var ?ApiNzMegaResponseNodelistInterface $nodelist */
		$nodelist = null;
		
		foreach($response as $innerCollection)
		{
			$nodelist = $innerCollection;
		}
		
		if(null === $nodelist)
		{
			$message = 'Failed to get inner collection from mega at url {url}';
			$context = ['{url}' => $url];
			
			throw new ApiNzMegaException(\strtr($message, $context), ApiNzMegaException::EARGS);
		}
		
		$rootFolder = $nodelist->getRootNode();
		
		$clearFolder = $rootFolder->decode($this->_containerKey);
		
		$hierarchy = new ApiNzMegaHierarchy(new ApiNzMegaHierarchyNode($clearFolder));
		
		/** @var ApiNzMegaResponseNode $otherFolder */
		foreach($nodelist->getNonrootNodes() as $otherFolder)
		{
			$otherClear = $otherFolder->decode($this->_containerKey);
			
			$hierarchy->add($otherClear);
		}
		
		return $this->_hierarchy = $hierarchy;
	}
	
}
