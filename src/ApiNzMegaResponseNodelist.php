<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiNzMega;

/**
 * ApiNzMegaResponseNodeList class file.
 *
 * This class represents a node list, as given by the API.
 *
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariableName")
 */
class ApiNzMegaResponseNodelist implements ApiNzMegaResponseNodelistInterface
{
	
	/**
	 * The nodes inside the response.
	 *
	 * @var array<integer, ApiNzMegaResponseNodeInterface>
	 */
	protected array $_nodes = [];
	
	/**
	 * unknown data.
	 *
	 * @var ApiNzMegaUserIdInterface
	 */
	protected ApiNzMegaUserIdInterface $_sn;
	
	/**
	 * unknown data.
	 *
	 * @var ?string
	 */
	protected ?string $_noc = null;
	
	/**
	 * unknown data.
	 * 
	 * @var ?string
	 */
	protected ?string $_st = null;
	
	/**
	 * Builds a new ApiNzMegaResponseNodelist with its inner data.
	 * 
	 * @param array<integer, ApiNzMegaResponseNodeInterface> $f
	 * @param ApiNzMegaUserIdInterface $sn
	 * @param ?string $noc
	 * @param ?string $st
	 */
	public function __construct(array $f, ApiNzMegaUserIdInterface $sn, ?string $noc, ?string $st)
	{
		$this->_nodes = $f;
		$this->_sn = $sn;
		$this->_noc = $noc;
		$this->_st = $st;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseNodelistInterface::getNodes()
	 */
	public function getNodes() : array
	{
		return $this->_nodes;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseNodelistInterface::getRootNode()
	 */
	public function getRootNode() : ApiNzMegaResponseNodeInterface
	{
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		return $this->_nodes[0];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiNzMega\ApiNzMegaResponseNodelistInterface::getNonrootNodes()
	 */
	public function getNonrootNodes() : array
	{
		return \array_slice($this->_nodes, 1);
	}
	
}
