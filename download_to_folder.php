<?php declare(strict_types=1);

/*
 * Example script on how to download recursively a folder from mega.
 * This script to launch from command line needs 2 arguments :
 * - A valid url to Mega, with full fragment (with the folder id and the key)
 * - A valid path on your system where to put all the downloaded data
 *
 * This script will create a mirror folder hierarchy as the one on Mega on
 * your filesystem, and put all folders names and file contents in clear.
 */

ini_set('memory_limit', '2048M');

use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\StringStream;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use PhpExtended\ApiNzMega\ApiNzMegaEndpoint;

global $argv;

if(!isset($argv[1]))
{
	throw new Exception('The first argument must be the url fragment of Mega folder.');
}
if(!isset($argv[2]))
{
	throw new Exception('The second argument must be a folder path on your fs to store the data.');
}

$urlFragment = $argv[1];
$target_path = $argv[2];
$realpath = realpath($target_path);
if($realpath === false)
{
	throw new Exception('The path "'.$target_path.'" does not exists.');
}
if(!is_writable($realpath))
{
	throw new Exception('The path "'.$realpath.'" is not writeable.');
}

echo "\n";
echo "\t".'Mega  Folder : '.$urlFragment."\n";
echo "\t".'Local Folder : '.$realpath."\n";
echo "\n";

$composer = __DIR__.'/vendor/autoload.php';
if(!is_file($composer))
{
	throw new Exception('The composer vendor directory is not up. Please run composer first.');
}
require_once $composer;

$client = new class implements ClientInterface {
	
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		echo date('Y-m-d H:i:s')." SENDING\n";
		echo $request->getMethod().' '.$request->getUri()->__toString()."\n";
		$headers = [];
		
		foreach($request->getHeaders() as $headerKey => $headerArr)
		{
			echo "\t".$headerKey.': '.\implode(', ', $headerArr)."\n";
			$headers[] = $headerKey.': '.\implode(', ', $headerArr);
		}
		
		echo "BODY: ".$request->getBody()->__toString()."\n";
		
		$context = \stream_context_create(['http' => [
			'protocol_version' => 1.1,
			'method' => $request->getMethod(),
			'header' => \implode("\r\n", $headers)."\r\n",
			'content' => $request->getBody()->__toString(),
		]]);
		
		$http_response_header = [];
		
		$data = (string) \file_get_contents($request->getUri()->__toString(), false, $context);
		
		echo date('Y-m-d H:i:s')." RECEIVED\n";
		$gzdecode = false;
		
		foreach($http_response_header as $headerval)
		{
			if(stripos($headerval, 'Content-Encoding: gzip') !== false)
			{
				$gzdecode = true;
			}
		}
		
		if($gzdecode)
		{
			$data = \gzdecode($data);
		}
		
		// \var_dump($data);
		
		return (new Response())->withBody(new StringStream($data));
	}
	
};

$mega = new ApiNzMegaEndpoint($urlFragment, $client);

echo "Getting node hierarchy info from Mega...\n\n";
$root = $mega->getRootNodeInfo();

$mega->downloadSynchronize($root, $realpath);
