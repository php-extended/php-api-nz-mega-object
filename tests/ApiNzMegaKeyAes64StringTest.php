<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaException;
use PhpExtended\ApiNzMega\ApiNzMegaKeyAes64Array32;
use PhpExtended\ApiNzMega\ApiNzMegaKeyAes64String;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaKeyAes64StringTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaKeyAes64String
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaKeyAes64StringTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaKeyAes64String
	 */
	protected ApiNzMegaKeyAes64String $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('randtext', $this->_object->__toString());
	}
	
	public function testToArray32() : void
	{
		$this->assertEquals(new ApiNzMegaKeyAes64Array32([1918987876, 1952807028]), $this->_object->toArray32());
	}
	
	public function testToRawString() : void
	{
		$this->assertEquals($this->_object, $this->_object->toRawString());
	}
	
	public function testCreateEmpty() : void
	{
		$this->expectException(ApiNzMegaException::class);
		
		new ApiNzMegaKeyAes64String('');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaKeyAes64String('randtext');
	}
	
}
