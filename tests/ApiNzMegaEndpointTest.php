<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaEndpoint;
use PhpExtended\ApiNzMega\ApiNzMegaNodeInterface;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\StringStream;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * ApiNzMegaEndpointTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaEndpoint
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaEndpointTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaEndpoint
	 */
	protected ApiNzMegaEndpoint $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetRootNodeInfo() : void
	{
		$this->assertInstanceOf(ApiNzMegaNodeInterface::class, $this->_object->getRootNodeInfo());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface
		{
			
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				// echo "\n".$request->getMethod().' '.$request->getUri()->__toString()."\n";
				$headers = [];
				
				foreach($request->getHeaders() as $headerKey => $headerArr)
				{
					// echo "\t".$headerKey.': '.\implode(', ', $headerArr)."\n";
					$headers[] = $headerKey.': '.\implode(', ', $headerArr);
				}
				
				// echo "BODY: ".$request->getBody()->__toString()."\n";
				
				$context = \stream_context_create(['http' => [
					'protocol_version' => 1.1,
					'method' => $request->getMethod(),
					'header' => \implode("\r\n", $headers)."\r\n",
					'content' => $request->getBody()->__toString(),
				]]);
				
				$data = (string) \file_get_contents($request->getUri()->__toString(), false, $context);
				$data = \gzdecode($data);
				
				// \var_dump($data);
				
				return (new Response())->withBody(new StringStream($data));
			}
			
		};
		
		$this->_object = new ApiNzMegaEndpoint('/7w12XQYI#1wucMyjfuO3VKfE005YciQ', $client);
	}
	
}
