<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaBase64String;
use PhpExtended\ApiNzMega\ApiNzMegaNodeId;
use PhpExtended\ApiNzMega\ApiNzMegaResponseKey;
use PhpExtended\ApiNzMega\ApiNzMegaResponseNode;
use PhpExtended\ApiNzMega\ApiNzMegaResponseNodelist;
use PhpExtended\ApiNzMega\ApiNzMegaUserId;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaResponseNodelistTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaResponseNodelist
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaResponseNodelistTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaResponseNodelist
	 */
	protected ApiNzMegaResponseNodelist $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetNodes() : void
	{
		$expected = [
			new ApiNzMegaResponseNode(
				new ApiNzMegaNodeId('ABCDEFGH'),
				new ApiNzMegaNodeId('IJKLMNOP'),
				new ApiNzMegaUserId('abcdefghijk'),
				12,
				new ApiNzMegaBase64String('abcdefghijkl'),
				new ApiNzMegaResponseKey('JKLMNOPQ:QWERTYUIOPASDFGHJKLMZXCVBN'),
				14,
				DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01'),
				'fake data',
			),
		];
		$this->assertEquals($expected, $this->_object->getNodes());
	}
	
	public function testGetRootNode() : void
	{
		$expected = new ApiNzMegaResponseNode(
			new ApiNzMegaNodeId('ABCDEFGH'),
			new ApiNzMegaNodeId('IJKLMNOP'),
			new ApiNzMegaUserId('abcdefghijk'),
			12,
			new ApiNzMegaBase64String('abcdefghijkl'),
			new ApiNzMegaResponseKey('JKLMNOPQ:QWERTYUIOPASDFGHJKLMZXCVBN'),
			14,
			DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01'),
			'fake data',
		);
		$this->assertEquals($expected, $this->_object->getRootNode());
	}
	
	public function testGetNonrootNodes() : void
	{
		$this->assertEquals([], $this->_object->getNonrootNodes());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaResponseNodelist([
			new ApiNzMegaResponseNode(
				new ApiNzMegaNodeId('ABCDEFGH'),
				new ApiNzMegaNodeId('IJKLMNOP'),
				new ApiNzMegaUserId('abcdefghijk'),
				12,
				new ApiNzMegaBase64String('abcdefghijkl'),
				new ApiNzMegaResponseKey('JKLMNOPQ:QWERTYUIOPASDFGHJKLMZXCVBN'),
				14,
				DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01'),
				'fake data',
			),
		], new ApiNzMegaUserId('lmnopqrstuv'), null, '}@sf#&g');
	}
	
}
