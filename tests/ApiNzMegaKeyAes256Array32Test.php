<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaException;
use PhpExtended\ApiNzMega\ApiNzMegaKeyAes128Array32;
use PhpExtended\ApiNzMega\ApiNzMegaKeyAes256Array32;
use PhpExtended\ApiNzMega\ApiNzMegaKeyAes256String;
use PhpExtended\ApiNzMega\ApiNzMegaKeyAes64Array32;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaKeyAes256Array32Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaKeyAes256Array32
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaKeyAes256Array32Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaKeyAes256Array32
	 */
	protected ApiNzMegaKeyAes256Array32 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('1952805748,1937011305,1852274537,1953000815,1919246953,1953721965,1869768046,1836020325', $this->_object->__toString());
	}
	
	public function testToArray32() : void
	{
		$this->assertEquals($this->_object, $this->_object->toArray32());
	}
	
	public function testToRawString() : void
	{
		$this->assertEquals(new ApiNzMegaKeyAes256String('teststringwithmorebitsnmorenmore'), $this->_object->toRawString());
	}
	
	public function testReduceAes128() : void
	{
		$this->assertEquals(new ApiNzMegaKeyAes128Array32([100667677, 117906436, 18158087, 419897098]), $this->_object->reduceAes128());
	}
	
	public function testGetInitializationVector() : void
	{
		$this->assertEquals(new ApiNzMegaKeyAes128Array32([1919246953, 1953721965, 0, 0]), $this->_object->getInitializationVector());
	}
	
	public function testGetMetaMac() : void
	{
		$this->assertEquals(new ApiNzMegaKeyAes64Array32([1869768046, 1836020325]), $this->_object->getMetaMac());
	}
	
	public function testCreateEmpty() : void
	{
		$this->expectException(ApiNzMegaException::class);
		
		new ApiNzMegaKeyAes256Array32([]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaKeyAes256Array32([1952805748, 1937011305, 1852274537, 1953000815, 1919246953, 1953721965, 1869768046, 1836020325]);
	}
	
}
