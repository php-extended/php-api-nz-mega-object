<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaBase64StringParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaBase64StringParserTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaBase64StringParser
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaBase64StringParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaBase64StringParser
	 */
	protected ApiNzMegaBase64StringParser $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaBase64StringParser();
	}
	
}
