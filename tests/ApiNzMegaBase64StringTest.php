<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaBase64String;
use PhpExtended\ApiNzMega\ApiNzMegaClearString;
use PhpExtended\ApiNzMega\ApiNzMegaException;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaBase64StringTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaBase64String
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaBase64StringTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaBase64String
	 */
	protected ApiNzMegaBase64String $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('cmFuZG9tIHRleHQ=', $this->_object->__toString());
	}
	
	public function testToBase64() : void
	{
		$this->assertEquals($this->_object, $this->_object->toBase64());
	}
	
	public function testToClear() : void
	{
		$this->assertEquals(new ApiNzMegaClearString('random text'), $this->_object->toClear());
	}
	
	public function testEmptyString() : void
	{
		$this->expectException(ApiNzMegaException::class);
		
		new ApiNzMegaBase64String('');
	}
	
	public function testNB64String() : void
	{
		$this->expectException(ApiNzMegaException::class);
		
		new ApiNzMegaBase64String('abcde');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaBase64String('cmFuZG9tIHRleHQ');
	}
	
}
