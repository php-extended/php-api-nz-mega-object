<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaBase64String;
use PhpExtended\ApiNzMega\ApiNzMegaClearString;
use PhpExtended\ApiNzMega\ApiNzMegaException;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaClearStringTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaClearString
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaClearStringTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaClearString
	 */
	protected ApiNzMegaClearString $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('random text', $this->_object->__toString());
	}
	
	public function testToBase64() : void
	{
		$this->assertEquals(new ApiNzMegaBase64String('cmFuZG9tIHRleHQ'), $this->_object->toBase64());
	}
	
	public function testToClear() : void
	{
		$this->assertEquals($this->_object, $this->_object->toClear());
	}
	
	public function testEmptyString() : void
	{
		$this->expectException(ApiNzMegaException::class);
		
		new ApiNzMegaClearString('');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaClearString('random text');
	}
	
}
