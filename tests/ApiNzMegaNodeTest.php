<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaAttribute;
use PhpExtended\ApiNzMega\ApiNzMegaKeyAes128String;
use PhpExtended\ApiNzMega\ApiNzMegaKeyAes64String;
use PhpExtended\ApiNzMega\ApiNzMegaNode;
use PhpExtended\ApiNzMega\ApiNzMegaNodeId;
use PhpExtended\ApiNzMega\ApiNzMegaUserId;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaNodeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaNode
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaNodeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaNode
	 */
	protected ApiNzMegaNode $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetNodeId() : void
	{
		$this->assertEquals(new ApiNzMegaNodeId('abcdefgh'), $this->_object->getNodeId());
	}
	
	public function testGetParentId() : void
	{
		$this->assertEquals(new ApiNzMegaNodeId('ijklmnop'), $this->_object->getParentId());
	}
	
	public function testGetOwnerId() : void
	{
		$this->assertEquals(new ApiNzMegaUserId('abcdefghijk'), $this->_object->getOwnerId());
	}
	
	public function testGetAttributes() : void
	{
		$this->assertEquals(new ApiNzMegaAttribute('name', 'value'), $this->_object->getAttributes());
	}
	
	public function testGetNodeType() : void
	{
		$this->assertEquals(1, $this->_object->getNodeType());
	}
	
	public function testGetNodeSize() : void
	{
		$this->assertEquals(2, $this->_object->getNodeSize());
	}
	
	public function testGetLastModifiedDate() : void
	{
		$this->assertEquals('2001-01-01', $this->_object->getLastModifiedDate()->format('Y-m-d'));
	}
	
	public function testGetNodeKey() : void
	{
		$this->assertEquals(new ApiNzMegaKeyAes128String('fullfeatunodekey'), $this->_object->getNodeKey());
	}
	
	public function testGetInitVec() : void
	{
		$this->assertEquals(new ApiNzMegaKeyAes128String('initializtvector'), $this->_object->getInitializationVector());
	}
	
	public function testGetMetaMac() : void
	{
		$this->assertEquals(new ApiNzMegaKeyAes64String('metamacc'), $this->_object->getMetaMac());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaNode(
			new ApiNzMegaNodeId('abcdefgh'),
			new ApiNzMegaNodeId('ijklmnop'),
			new ApiNzMegaUserId('abcdefghijk'),
			new ApiNzMegaAttribute('name', 'value'),
			1,
			2,
			DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01'),
			new ApiNzMegaKeyAes128String('fullfeatunodekey'),
			new ApiNzMegaKeyAes128String('initializtvector'),
			new ApiNzMegaKeyAes64String('metamacc'),
		);
	}
	
}
