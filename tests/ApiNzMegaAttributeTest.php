<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaAttribute;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaAttributeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaAttribute
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaAttributeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaAttribute
	 */
	protected ApiNzMegaAttribute $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('key', $this->_object->getName());
	}
	
	public function testGetData() : void
	{
		$this->assertEquals('value', $this->_object->getData());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaAttribute('key', 'value');
	}
	
}
