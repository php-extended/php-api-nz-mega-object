<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaBase64String;
use PhpExtended\ApiNzMega\ApiNzMegaException;
use PhpExtended\ApiNzMega\ApiNzMegaNodeId;
use PhpExtended\ApiNzMega\ApiNzMegaResponseKey;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaResponseKeyTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaResponseKey
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaResponseKeyTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaResponseKey
	 */
	protected ApiNzMegaResponseKey $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetNodeId() : void
	{
		$this->assertEquals(new ApiNzMegaNodeId('abcdefgh'), $this->_object->getNodeId());
	}
	
	public function testGetNodeKey() : void
	{
		$this->assertEquals(new ApiNzMegaBase64String('abcdefghijklmnop'), $this->_object->getNodeKey());
	}
	
	public function testCreateNoSemicolon() : void
	{
		$this->expectException(ApiNzMegaException::class);
		
		new ApiNzMegaResponseKey('abcdefgh');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaResponseKey('abcdefgh:abcdefghijklmnop');
	}
	
}
