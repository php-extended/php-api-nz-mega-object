<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaException;
use PhpExtended\ApiNzMega\ApiNzMegaKeyAes128Array32;
use PhpExtended\ApiNzMega\ApiNzMegaKeyAes128String;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaKeyAes128Array32Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaKeyAes128Array32
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaKeyAes128Array32Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaKeyAes128Array32
	 */
	protected ApiNzMegaKeyAes128Array32 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('1952805748,1801812324,1635017078,1634497893', $this->_object->__toString());
	}
	
	public function testToArray32() : void
	{
		$this->assertEquals($this->_object, $this->_object->toArray32());
	}
	
	public function testToRawString() : void
	{
		$this->assertEquals(new ApiNzMegaKeyAes128String('testkeydatavalue'), $this->_object->toRawString());
	}
	
	public function testBuildWrongQuantity() : void
	{
		$this->expectException(ApiNzMegaException::class);
		
		new ApiNzMegaKeyAes128Array32([]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaKeyAes128Array32([1952805748, 1801812324, 1635017078, 1634497893]);
	}
	
}
