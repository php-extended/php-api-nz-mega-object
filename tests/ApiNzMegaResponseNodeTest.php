<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaBase64String;
use PhpExtended\ApiNzMega\ApiNzMegaNodeId;
use PhpExtended\ApiNzMega\ApiNzMegaResponseKey;
use PhpExtended\ApiNzMega\ApiNzMegaResponseNode;
use PhpExtended\ApiNzMega\ApiNzMegaUserId;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaResponseNodeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaResponseNode
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaResponseNodeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaResponseNode
	 */
	protected ApiNzMegaResponseNode $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetNodeId() : void
	{
		$this->assertEquals(new ApiNzMegaNodeId('ABCDEFGH'), $this->_object->getNodeId());
	}
	
	public function testGetParentNodeId() : void
	{
		$this->assertEquals(new ApiNzMegaNodeId('IJKLMNOP'), $this->_object->getParentNodeId());
	}
	
	public function testGetOwnerId() : void
	{
		$this->assertEquals(new ApiNzMegaUserId('abcdefghijk'), $this->_object->getOwnerId());
	}
	
	public function testGetNodeType() : void
	{
		$this->assertEquals(12, $this->_object->getNodeType());
	}
	
	public function testGetNodeKey() : void
	{
		$this->assertEquals(new ApiNzMegaResponseKey('JKLMNOPQ:QWERTYUIOPASDFGHJKLMZXCVBN'), $this->_object->getNodeKey());
	}
	
	public function testGetNodeSize() : void
	{
		$this->assertEquals(14, $this->_object->getNodeSize());
	}
	
	public function testGetLastModifiedStart() : void
	{
		$this->assertEquals('2001-01-01', $this->_object->getLastModifiedDatetime()->format('Y-m-d'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaResponseNode(
			new ApiNzMegaNodeId('ABCDEFGH'),
			new ApiNzMegaNodeId('IJKLMNOP'),
			new ApiNzMegaUserId('abcdefghijk'),
			12,
			new ApiNzMegaBase64String('abcdefghijkl'),
			new ApiNzMegaResponseKey('JKLMNOPQ:QWERTYUIOPASDFGHJKLMZXCVBN'),
			14,
			DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01'),
			'fake data',
		);
	}
	
}
