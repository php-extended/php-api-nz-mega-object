<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaNodeIdParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaNodeIdParserTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaNodeIdParser
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaNodeIdParserTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaNodeIdParser
	 */
	protected ApiNzMegaNodeIdParser $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaNodeIdParser();
	}
	
}
