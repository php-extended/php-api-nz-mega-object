<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaAttribute;
use PhpExtended\ApiNzMega\ApiNzMegaHierarchyNode;
use PhpExtended\ApiNzMega\ApiNzMegaKeyAes128String;
use PhpExtended\ApiNzMega\ApiNzMegaKeyAes64String;
use PhpExtended\ApiNzMega\ApiNzMegaNode;
use PhpExtended\ApiNzMega\ApiNzMegaNodeId;
use PhpExtended\ApiNzMega\ApiNzMegaUserId;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaHierarchyNodeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaHierarchyNode
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaHierarchyNodeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaHierarchyNode
	 */
	protected ApiNzMegaHierarchyNode $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetNode() : void
	{
		$expected = new ApiNzMegaNode(
			new ApiNzMegaNodeId('abcdefgh'),
			new ApiNzMegaNodeId('ijklmnop'),
			new ApiNzMegaUserId('abcdefghijk'),
			new ApiNzMegaAttribute('name', 'value'),
			1,
			2,
			DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01'),
			new ApiNzMegaKeyAes128String('fullfeatunodekey'),
			new ApiNzMegaKeyAes128String('initializtvector'),
			new ApiNzMegaKeyAes64String('metamacc'),
		);
		$this->assertEquals($expected, $this->_object->getNode());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaHierarchyNode(
			new ApiNzMegaNode(
				new ApiNzMegaNodeId('abcdefgh'),
				new ApiNzMegaNodeId('ijklmnop'),
				new ApiNzMegaUserId('abcdefghijk'),
				new ApiNzMegaAttribute('name', 'value'),
				1,
				2,
				DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01'),
				new ApiNzMegaKeyAes128String('fullfeatunodekey'),
				new ApiNzMegaKeyAes128String('initializtvector'),
				new ApiNzMegaKeyAes64String('metamacc'),
			),
		);
	}
	
}
