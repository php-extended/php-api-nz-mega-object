<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaException;
use PhpExtended\ApiNzMega\ApiNzMegaNodeId;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaNodeIdTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaNodeId
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaNodeIdTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaNodeId
	 */
	protected ApiNzMegaNodeId $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('abcdefgh', $this->_object->__toString());
	}
	
	public function testGetValue() : void
	{
		$this->assertEquals('abcdefgh', $this->_object->getValue());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals($this->_object));
	}
	
	public function testCreateEmpty() : void
	{
		$this->expectException(ApiNzMegaException::class);
		
		new ApiNzMegaNodeId('');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaNodeId('abcdefgh');
	}
	
}
