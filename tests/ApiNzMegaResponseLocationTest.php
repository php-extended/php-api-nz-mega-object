<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaBase64String;
use PhpExtended\ApiNzMega\ApiNzMegaResponseLocation;
use PhpExtended\HttpMessage\Uri;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaResponseLocationTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaResponseLocation
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaResponseLocationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaResponseLocation
	 */
	protected ApiNzMegaResponseLocation $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetSize() : void
	{
		$this->assertEquals(12, $this->_object->getSize());
	}
	
	public function testGetFileName() : void
	{
		$this->assertEquals(new ApiNzMegaBase64String('abcdefgh'), $this->_object->getFileName());
	}
	
	public function testGetMsd() : void
	{
		$this->assertEquals(14, $this->_object->getMsd());
	}
	
	public function testGetFa() : void
	{
		$this->assertEquals('fake data', $this->_object->getFa());
	}
	
	public function testGetTimeToLive() : void
	{
		$this->assertEquals(16, $this->_object->getTimeToLive());
	}
	
	public function testGetTargetUri() : void
	{
		$this->assertEquals(new Uri(), $this->_object->getTargetUri());
	}
	
	public function testGetIpAddresses() : void
	{
		$this->assertEquals(['10.0.0.1'], $this->_object->getIpAddresses());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaResponseLocation(
			12,
			new ApiNzMegaBase64String('abcdefgh'),
			14,
			'fake data',
			16,
			new Uri(),
			['10.0.0.1'],
		);
	}
	
}
