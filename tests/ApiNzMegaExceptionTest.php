<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaException;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaException
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaException
	 */
	protected ApiNzMegaException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(\get_class($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaException(null, 12);
	}
	
}
