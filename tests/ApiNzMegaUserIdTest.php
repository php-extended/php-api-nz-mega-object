<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-nz-mega-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiNzMega\ApiNzMegaException;
use PhpExtended\ApiNzMega\ApiNzMegaUserId;
use PHPUnit\Framework\TestCase;

/**
 * ApiNzMegaUserIdTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiNzMega\ApiNzMegaUserId
 *
 * @internal
 *
 * @small
 */
class ApiNzMegaUserIdTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiNzMegaUserId
	 */
	protected ApiNzMegaUserId $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetValue() : void
	{
		$this->assertEquals('ABCDEFGHIJK', $this->_object->getValue());
	}
	
	public function testBuildFailed() : void
	{
		$this->expectException(ApiNzMegaException::class);
		
		new ApiNzMegaUserId('');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiNzMegaUserId('ABCDEFGHIJK');
	}
	
}
